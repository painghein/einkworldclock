package com.gajah.einkworldclock;

import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;

public class SwipeHelper extends SimpleOnGestureListener implements
		View.OnTouchListener {

	final String LogUtilTag = "ActivitySwipeDetector";
	private int SWIPE_MIN_DISTANCE = 100;
	private int SWIPE_MAX_OFF_PATH = 250;
	private int SWIPE_THRESHOLD_VELOCITY = 200;
	// private float downX, downY, upX, upY;
	GestureDetector gDetector;

	public SwipeHelper(DisplayMetrics dm) {
		super();
		SWIPE_MIN_DISTANCE = (int) (SWIPE_MIN_DISTANCE * dm.density);
		SWIPE_MAX_OFF_PATH = (int) (SWIPE_MAX_OFF_PATH * dm.density);
		SWIPE_THRESHOLD_VELOCITY = (int) (SWIPE_THRESHOLD_VELOCITY * dm.density);
		gDetector = new GestureDetector(this);
	}

	public void onRightToLeftSwipe() {
	}

	public void onLeftToRightSwipe() {
	}

	public void requestFocus() {
	}

	public void onTopToBottomSwipe() {
		LogUtil.i(LogUtilTag, "onTopToBottomSwipe!");
	}

	public void onBottomToTopSwipe() {
		LogUtil.i(LogUtilTag, "onBottomToTopSwipe!");
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e) {
		return super.onSingleTapConfirmed(e);
	}

	public boolean onTouch(View v, MotionEvent event) {
		requestFocus();
		// Within the MyGestureListener class you can now manage the
		// event.getAction() codes.

		// Note that we are now calling the gesture Detectors onTouchEvent. And
		// given we've set this class as the GestureDetectors listener
		// the onFling, onSingleTap etc methods will be executed.
		return gDetector.onTouchEvent(event);
	}

	public GestureDetector getDetector() {
		return gDetector;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		LogUtil.i(LogUtilTag, "ONFling!");
		try {
			if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
				return false;
			// right to left swipe
			if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
					&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
				onLeftToRightSwipe();
			} else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
					&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
				onRightToLeftSwipe();
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
