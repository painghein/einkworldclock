/* Copyright (C) 2012 Gajah Digital Media
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gajah.einkworldclock.adapters;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.gajah.einkworldclock.LogUtil;

public class AdapterPageList extends BaseAdapter {

	private LayoutInflater mInflater;
	private int resource;
	private List<? extends Map<String, ?>> data;
	private String[] from;
	private int[] to;
	public int VIEW_COUNT;
	private int totalPage;
	private int index;
	private int pre_total;

	public AdapterPageList(Context ctx, List<? extends Map<String, ?>> data,
			int resouce, String[] from, int[] to, int num) {
		this.data = data;
		this.resource = resouce;
		this.data = data;
		this.from = from;
		this.to = to;
		index = 1;
		pre_total = 0;
		VIEW_COUNT = num;
		totalPage = (data.size() % VIEW_COUNT) == 0 ? (data.size() / VIEW_COUNT)
				: (data.size() / VIEW_COUNT) + 1;
		mInflater = (LayoutInflater) ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	public void setIndex(int i) {
		index = i;
	}

	public int getIndex() {
		return index;
	}

	public int getTotalpage() {
		return totalPage;
	}

	public int getCount() {
		pre_total = (index - 1) * VIEW_COUNT;
		if (data.size() - pre_total < VIEW_COUNT)
			return data.size() - pre_total;
		else
			return VIEW_COUNT;
	}

	public Object getItem(int position) {
		return data.get(pre_total + position);
	}

	@SuppressWarnings("unchecked")
	public String get(int position, Object key) {
		Map<String, ?> map = (Map<String, ?>) getItem(pre_total + position);
		return map.get(key).toString();
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null)
			convertView = mInflater.inflate(resource, null);
		Map<String, ?> item = data.get(pre_total + position);
		int count = to.length;
		LogUtil.I("----------Activity-----------"
				+ (item == null ? " null " : item.toString()));
		for (int i = 0; i < count; i++) {
			View v = convertView.findViewById(to[i]);
			bindView(v, item, from[i]);
		}
		convertView.setTag(pre_total + position);
		return convertView;
	}

	/**
	 * @param view
	 * @param item
	 * @param from
	 */
	private void bindView(View view, Map<String, ?> item, String from) {
		Object data = (Object) item.get(from);
		if (view instanceof CheckBox)
			((CheckBox) view).setChecked("true".equals(data) ? true : false);
		else if (view instanceof TextView) {
			((TextView) view).setText(data == null ? "" : Html.fromHtml(data
					.toString()));
		}

	}

	public long getItemId(int position) {
		return position;
	}
}
