/* Copyright (C) 2012 Gajah Digital Media
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gajah.einkworldclock.adapters;

import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gajah.einkworldclock.R;
import com.gajah.einkworldclock.views.AnalogClock;
import com.gajah.einkworldclock.views.DigitalClock;

public class TimeZoneListAdapter extends BaseAdapter {

	private LayoutInflater mInflater;
	private int resource;
	private List<? extends Map<String, ?>> data;
	private String[] from;
	public int VIEW_COUNT;
	private int totalPage;
	private int index;
	private int pre_total;

	public TimeZoneListAdapter(Context ctx,
			List<? extends Map<String, ?>> data, int resouce, String[] from,
			int num) {
		this.data = data;
		this.resource = resouce;
		this.data = data;
		this.from = from;

		index = 1;
		pre_total = 0;
		VIEW_COUNT = num;
		totalPage = (data.size() % VIEW_COUNT) == 0 ? (data.size() / VIEW_COUNT)
				: (data.size() / VIEW_COUNT) + 1;
		mInflater = (LayoutInflater) ctx
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	public void setIndex(int i) {
		index = i;
	}

	public int getIndex() {
		return index;
	}

	public int getTotalpage() {
		return totalPage;
	}

	public int getCount() {
		pre_total = (index - 1) * VIEW_COUNT;
		if (data.size() - pre_total < VIEW_COUNT)
			return data.size() - pre_total;
		else
			return VIEW_COUNT;
	}

	public Object getItem(int position) {
		return data.get(pre_total + position);
	}

	@SuppressWarnings("unchecked")
	public String get(int position, Object key) {
		Map<String, ?> map = (Map<String, ?>) getItem(pre_total + position);
		return map.get(key).toString();
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		Map<String, ?> item = data.get(pre_total + position);
		View view = convertView;
		ViewHolder holder;
		if (view == null) {
			view = mInflater.inflate(resource, null);
			holder = new ViewHolder();
			holder.tvName = (TextView) view.findViewById(R.id.tvCity);
			holder.analogClock = (AnalogClock) view
					.findViewById(R.id.analogClock);
			holder.digitalClock = (DigitalClock) view
					.findViewById(R.id.digitalClock);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}

		/**
		 * This area may not be suitable to general use. For performance reason,
		 * data are extracted directly from Hashmap assuming activity will
		 * supply data in order
		 */
		holder.tvName.setText(item.get(from[0]).toString());
		holder.analogClock.setTimeZone((TimeZone) (item.get(from[1])));
		holder.digitalClock.setTimeZone((TimeZone) (item.get(from[2])));
		/***********************************************************************/
		return view;
	}

	public long getItemId(int position) {
		return position;
	}

	static class ViewHolder {
		TextView tvName;
		AnalogClock analogClock;
		DigitalClock digitalClock;
	}

}
