/*
 * /*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * 
 * 
 * Copyright (C) 2012 Gajah Digital Media
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gajah.einkworldclock;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.gajah.einksdk.EinkKeyEvent;
import com.gajah.einkworldclock.adapters.AdapterPageList;
import com.gajah.einkworldclock.provider.WorldClockProvider;

public class TimeZoneList extends Activity implements OnItemClickListener {
	private static final String TAG = "ZoneList";
	private static final String KEY_ID = "id";
	private static final String KEY_DISPLAYNAME = "name";
	private static final String KEY_GMT = "gmt";
	private static final String KEY_OFFSET = "offset";
	private static final String XMLTAG_TIMEZONE = "timezone";

	private static final int HOURS_1 = 60 * 60000;

	// Initial focus position
	private int mDefault;

	private boolean mSortedByTimezone;

	private AdapterPageList mTimezoneSortedAdapter;
	private AdapterPageList mAlphabeticalAdapter;

	private ListView list;
	MenuPopup demopw;
	private final int MSG_UPDATE_PAGE_NUM = 1;
	TextView mPagenum;

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case MSG_UPDATE_PAGE_NUM:
				mPagenum.setText((CharSequence) msg.obj);
				break;
			}
		}

	};

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.zone_list);
		list = (ListView) findViewById(R.id.list);
		mPagenum = (TextView) findViewById(R.id.tvPagenum);

		String[] from = new String[] { KEY_DISPLAYNAME, KEY_GMT };
		int[] to = new int[] { R.id.timezone_name, R.id.timezone_time };

		MyComparator comparator = new MyComparator(KEY_OFFSET);

		final List<HashMap> timezoneSortedList = getZones();
		Collections.sort(timezoneSortedList, comparator);
		mTimezoneSortedAdapter = new AdapterPageList(this,
				(List) timezoneSortedList, R.layout.timezone, from, to, 8);

		List<HashMap> alphabeticalList = new ArrayList<HashMap>(
				timezoneSortedList);
		comparator.setSortingKey(KEY_DISPLAYNAME);
		Collections.sort(alphabeticalList, comparator);
		mAlphabeticalAdapter = new AdapterPageList(this,
				(List) alphabeticalList, R.layout.timezone, from, to, 8);

		// Sets the adapter
		setSorting(true);

		// If current timezone is in this list, move focus to it
		list.setSelection(mDefault);
		list.setOnItemClickListener(this);
		list.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				int index = ((AdapterPageList) list.getAdapter()).getIndex();
				LogUtil.I("----------Activity::onKey-----------Key Code"
						+ event.getKeyCode());
				if ((keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == EinkKeyEvent.KEYCODE_PAGEDOWN)
						&& event.getAction() == KeyEvent.ACTION_DOWN) {
					if (index-- > 1) {
						LogUtil.I("----------mListview::onKey-----------pre"
								+ index);
						((AdapterPageList) list.getAdapter()).setIndex(index);
						((AdapterPageList) list.getAdapter())
								.notifyDataSetChanged();

						Object o = index
								+ "/"
								+ (((AdapterPageList) list.getAdapter())
										.getTotalpage() > 0 ? ((AdapterPageList) list
										.getAdapter()).getTotalpage() : 1);
						sendMessage(MSG_UPDATE_PAGE_NUM, o);
					}
					return true;
				} else if ((keyCode == KeyEvent.KEYCODE_DPAD_RIGHT || keyCode == EinkKeyEvent.KEYCODE_PAGEUP)
						&& event.getAction() == KeyEvent.ACTION_DOWN) {

					if ((float) index++ < (float) timezoneSortedList.size()
							/ ((AdapterPageList) list.getAdapter()).VIEW_COUNT) {
						LogUtil.I("----------mListview::onKey-----------next"
								+ index);
						((AdapterPageList) list.getAdapter()).setIndex(index);
						((AdapterPageList) list.getAdapter())
								.notifyDataSetChanged();

						Object o = index
								+ "/"
								+ (((AdapterPageList) list.getAdapter())
										.getTotalpage() > 0 ? ((AdapterPageList) list
										.getAdapter()).getTotalpage() : 1);
						sendMessage(MSG_UPDATE_PAGE_NUM, o);
					}
					return true;
				}
				return false;
			}
		});
		list.setOnTouchListener(new SwipeHelper(getResources()
				.getDisplayMetrics()) {
			@Override
			public void requestFocus() {
				list.requestFocus();
			}

			@Override
			public void onRightToLeftSwipe() {
				list.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,
						KeyEvent.KEYCODE_DPAD_LEFT));
			}

			@Override
			public void onLeftToRightSwipe() {
				list.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,
						KeyEvent.KEYCODE_DPAD_RIGHT));
			}

		});

		// Assume user may press Back
		setResult(RESULT_CANCELED);
	}

	private void setSorting(boolean timezone) {
		list.setAdapter(timezone ? mTimezoneSortedAdapter
				: mAlphabeticalAdapter);
		Object o = ((AdapterPageList) list.getAdapter()).getIndex()
				+ "/"
				+ (((AdapterPageList) list.getAdapter()).getTotalpage() > 0 ? ((AdapterPageList) list
						.getAdapter()).getTotalpage() : 1);
		sendMessage(MSG_UPDATE_PAGE_NUM, o);
		mSortedByTimezone = timezone;
	}

	@SuppressWarnings("rawtypes")
	private List<HashMap> getZones() {
		List<HashMap> myData = new ArrayList<HashMap>();
		long date = Calendar.getInstance().getTimeInMillis();
		try {
			XmlResourceParser xrp = getResources().getXml(R.xml.timezones);
			while (xrp.next() != XmlResourceParser.START_TAG)
				;
			xrp.next();
			while (xrp.getEventType() != XmlResourceParser.END_TAG) {
				while (xrp.getEventType() != XmlResourceParser.START_TAG) {
					if (xrp.getEventType() == XmlResourceParser.END_DOCUMENT) {
						return myData;
					}
					xrp.next();
				}
				if (xrp.getName().equals(XMLTAG_TIMEZONE)) {
					String id = xrp.getAttributeValue(0);
					String displayName = xrp.nextText();
					addItem(myData, id, displayName, date);
				}
				while (xrp.getEventType() != XmlResourceParser.END_TAG) {
					xrp.next();
				}
				xrp.next();
			}
			xrp.close();
		} catch (XmlPullParserException xppe) {
			Log.e(TAG, "Ill-formatted timezones.xml file");
		} catch (java.io.IOException ioe) {
			Log.e(TAG, "Unable to read timezones.xml file");
		}

		return myData;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void addItem(List<HashMap> myData, String id, String displayName,
			long date) {
		HashMap map = new HashMap();
		map.put(KEY_ID, id);
		map.put(KEY_DISPLAYNAME, displayName + " (" + getDiaplayName(id) + ")");
		TimeZone tz = TimeZone.getTimeZone(id);
		int offset = tz.getOffset(date);
		int p = Math.abs(offset);
		StringBuilder name = new StringBuilder();
		name.append("GMT");

		if (offset < 0) {
			name.append('-');
		} else {
			name.append('+');
		}

		name.append(p / (HOURS_1));
		name.append(':');

		int min = p / 60000;
		min %= 60;

		if (min < 10) {
			name.append('0');
		}
		name.append(min);

		map.put(KEY_GMT, name.toString());
		map.put(KEY_OFFSET, offset);

		if (id.equals(TimeZone.getDefault().getID())) {
			mDefault = myData.size();
		}

		myData.add(map);
	}

	@SuppressWarnings("rawtypes")
	private static class MyComparator implements Comparator<HashMap> {
		private String mSortingKey;

		public MyComparator(String sortingKey) {
			mSortingKey = sortingKey;
		}

		public void setSortingKey(String sortingKey) {
			mSortingKey = sortingKey;
		}

		@SuppressWarnings("unchecked")
		public int compare(HashMap map1, HashMap map2) {
			Object value1 = map1.get(mSortingKey);
			Object value2 = map2.get(mSortingKey);

			/*
			 * This should never happen, but just in-case, put non-comparable
			 * items at the end.
			 */
			if (!isComparable(value1)) {
				return isComparable(value2) ? 1 : 0;
			} else if (!isComparable(value2)) {
				return -1;
			}

			return ((Comparable) value1).compareTo(value2);
		}

		private boolean isComparable(Object value) {
			return (value != null) && (value instanceof Comparable);
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Map map = (Map) parent.getItemAtPosition(position);
		// Update the system timezone value
		WorldClockProvider dbHelper = new WorldClockProvider(this);
		dbHelper.open();
		dbHelper.createLocation((String) map.get(KEY_ID));
		dbHelper.close();
		setResult(RESULT_OK);
		finish();

	}

	public void sendMessage(int msgCode, Object o) {
		Message m = new Message();
		m.what = msgCode;
		m.obj = o;
		mHandler.sendMessage(m);
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {

		if (demopw != null && demopw.isShowing()) {
			return demopw.dispatchKeyEvent(event);
		} else if (event.getKeyCode() == KeyEvent.KEYCODE_MENU
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			if (demopw == null) {
				demopw = new MenuPopup(this);
				demopw.show(this.findViewById(R.id.test_demo), 3, 3);
			} else {
				demopw.show(this.findViewById(R.id.test_demo), 3, 3);
			}
		}
		return super.dispatchKeyEvent(event);
	}

	public class MenuPopup implements OnKeyListener {
		public PopupWindow demopopup;
		private ListView mListview;
		private View inflateView;

		private static final int SORT = 0;

		public MenuPopup(Activity context) {
			LayoutInflater inflate = LayoutInflater.from(context);
			inflateView = inflate.inflate(R.layout.menu_weather_list, null);
			mListview = (ListView) inflateView
					.findViewById(R.id.menu_popupwindow_listview);
			demopopup = new PopupWindow(inflateView, 300,
					LayoutParams.WRAP_CONTENT);
			demopopup.setFocusable(true);
		}

		public void initData() {
			String[] items;
			if (mSortedByTimezone) {
				items = getResources().getStringArray(R.array.menu_zonepicker);
			} else {
				items = getResources()
						.getStringArray(R.array.menu_zonepicker_1);
			}
			ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(
					TimeZoneList.this, R.layout.menu_list_item, items);
			mListview.setAdapter(listAdapter);
			mListview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			mListview.clearChoices();
			mListview.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					mListview.setItemChecked(position, true);
					dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,
							KeyEvent.KEYCODE_DPAD_CENTER));
				}

			});
			mListview.setOnKeyListener(this);
		}

		public void show(View parent, int x, int y) {
			initData();
			demopopup.showAtLocation(parent, Gravity.NO_GRAVITY, x, y);

		}

		public boolean isShowing() {
			if (demopopup != null)
				return demopopup.isShowing();
			return false;
		}

		public void dismiss() {
			if (demopopup != null)
				demopopup.dismiss();
			// demopopup=null;
		}

		public boolean dispatchKeyEvent(KeyEvent event) {
			// TODO Auto-generated method stub
			LogUtil.I("----------mListview::dispatchKeyEvent-----------"
					+ event.getKeyCode());

			if (event.getKeyCode() == KeyEvent.KEYCODE_BACK
					&& event.getAction() == KeyEvent.ACTION_DOWN) {
				dismiss();
				return true;
			} else if (event.getKeyCode() == KeyEvent.KEYCODE_MENU
					&& event.getAction() == KeyEvent.ACTION_DOWN) {
				dismiss();
				return true;
			} else if ((event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER)
					&& event.getAction() == KeyEvent.ACTION_DOWN) {
				int index = mListview.getCheckedItemPosition();
				if (index < 0) {
					index = mListview.getSelectedItemPosition();
				}
				LogUtil.E("Selected ID is " + index);
				switch (index) {
				case SORT:
					dismiss();
					setSorting(!mSortedByTimezone);
					break;

				}
				return true;
			}
			return false;
		}

		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			return dispatchKeyEvent(event);
		}
	}

	private String getDiaplayName(String timezonename) {
		String displayname = timezonename;
		int sep = timezonename.indexOf('/');

		if (-1 != sep) {
			displayname = timezonename.substring(sep + 1) + ", "
					+ timezonename.substring(0, sep);
			displayname = displayname.replace("_", " ");
		}

		return displayname;
	}

}