/* 
 * Copyright (C) 2012 Gajah Digital Media
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gajah.einkworldclock.provider;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.gajah.einkworldclock.LogUtil;

public final class WorldClockProvider {

	public static final String TAG = "Location Database Helper";

	public static final String TABLE_CITIES = "cities";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_NAME = "name";

	private SQLiteDatabase database;
	private MySQLiteHelper dbHelper;
	private String[] allColumns = { COLUMN_ID, COLUMN_NAME, };

	public WorldClockProvider(Context context) {
		dbHelper = new MySQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public Long createLocation(String name) {
		if (getAllLocations().contains(name)) {
			return (long) -1;
		}
		ContentValues values = new ContentValues();
		values.put(COLUMN_NAME, name);
		return database.insert(TABLE_CITIES, null, values);

	}

	public void deleteLocation(String name) {
		database.delete(TABLE_CITIES, COLUMN_NAME + " = '" + name + "'", null);
		LogUtil.i(TAG, "Comment deleted with id: " + name);
	}

	public List<String> getAllLocations() {
		List<String> comments = new ArrayList<String>();
		Cursor cursor = database.query(TABLE_CITIES, allColumns, null, null,
				null, null, null);
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			String comment = cursorToLocation(cursor);
			comments.add(comment);
			cursor.moveToNext();
		}
		// Make sure to close the cursor
		cursor.close();
		return comments;
	}

	private String cursorToLocation(Cursor cursor) {
		String location = "";
		location = cursor.getString(1);
		return location;
	}

	public class MySQLiteHelper extends SQLiteOpenHelper {

		private static final String DATABASE_NAME = "data.db";
		private static final int DATABASE_VERSION = 1;

		// Database creation sql statement
		private static final String DATABASE_CREATE = "create table "
				+ TABLE_CITIES + "( " + COLUMN_ID
				+ " integer primary key autoincrement, " + COLUMN_NAME
				+ " text not null);";

		public MySQLiteHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase database) {
			database.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			LogUtil.i(MySQLiteHelper.class.getName(),
					"Upgrading database from version " + oldVersion + " to "
							+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS" + TABLE_CITIES);
			onCreate(db);
		}

	}
}
