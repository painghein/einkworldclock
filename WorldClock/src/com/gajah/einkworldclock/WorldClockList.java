/* Copyright (C) 2012 Gajah Digital Media
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gajah.einkworldclock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.gajah.einksdk.EinkKeyEvent;
import com.gajah.einkworldclock.adapters.TimeZoneListAdapter;
import com.gajah.einkworldclock.provider.WorldClockProvider;

/**
 * Displays a list of notes. Will display notes from the {@link Uri} provided in
 * the intent if there is one, otherwise defaults to displaying the contents of
 * the {@link NotePadProvider}
 */
public class WorldClockList extends Activity {
	private static final String TAG = "World Clock List";

	private static final String LOCATION = "location";
	private static final String TIMEZONE = "timezone";
	private static final String TIMEZONE1 = "timezone1";

	private static final int VIEW_COUNT = 4;

	GridView grid;
	TextView mPagenum, tvError;
	ArrayList<HashMap<String, Object>> mdata;
	ArrayList<String> timeZoneStringList = new ArrayList<String>();
	private TimeZoneListAdapter listAdapter;
	private final int MSG_UPDATE_PAGE_NUM = 1;
	MenuPopup demopw;

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			switch (msg.what) {
			case MSG_UPDATE_PAGE_NUM:
				mPagenum.setText((CharSequence) msg.obj);
				break;
			}
		}

	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setDefaultKeyMode(DEFAULT_KEYS_SHORTCUT);

		// UI Links
		setContentView(R.layout.main);
		grid = (GridView) findViewById(R.id.grid);
		mPagenum = (TextView) findViewById(R.id.tvPagenum);
		tvError = (TextView) findViewById(R.id.tvError);

		// Hashmap to hold data
		mdata = new ArrayList<HashMap<String, Object>>();

		grid.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				int index = listAdapter.getIndex();
				LogUtil.I("----------Activity::onKey-----------Key Code"
						+ event.getKeyCode());
				if ((keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == EinkKeyEvent.KEYCODE_PAGEDOWN)
						&& event.getAction() == KeyEvent.ACTION_DOWN) {
					if (index-- > 1) {
						LogUtil.I("----------mListview::onKey-----------pre"
								+ index);
						listAdapter.setIndex(index);
						listAdapter.notifyDataSetChanged();

						Object o = index
								+ "/"
								+ (listAdapter.getTotalpage() > 0 ? listAdapter
										.getTotalpage() : 1);
						sendMessage(MSG_UPDATE_PAGE_NUM, o);
					}
					return true;
				} else if ((keyCode == KeyEvent.KEYCODE_DPAD_RIGHT || keyCode == EinkKeyEvent.KEYCODE_PAGEUP)
						&& event.getAction() == KeyEvent.ACTION_DOWN) {

					if ((float) index++ < (float) mdata.size()
							/ listAdapter.VIEW_COUNT) {
						LogUtil.I("----------mListview::onKey-----------next"
								+ index);
						listAdapter.setIndex(index);
						listAdapter.notifyDataSetChanged();

						Object o = index
								+ "/"
								+ (listAdapter.getTotalpage() > 0 ? listAdapter
										.getTotalpage() : 1);
						sendMessage(MSG_UPDATE_PAGE_NUM, o);
					}
					return true;
				}
				return false;
			}
		});
		
		grid.setOnTouchListener(new SwipeHelper(getResources()
				.getDisplayMetrics()) {
			@Override
			public void requestFocus() {
				grid.requestFocus();
			}

			@Override
			public void onRightToLeftSwipe() {
				grid.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,
						KeyEvent.KEYCODE_DPAD_LEFT));
			}

			@Override
			public void onLeftToRightSwipe() {
				grid.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,
						KeyEvent.KEYCODE_DPAD_RIGHT));
			}

		});
		registerForContextMenu(grid);

		// start background task to do data retrieving
		GetInfoTask getInfoTask = new GetInfoTask();
		getInfoTask.execute();
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		if (v == grid) {
			AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
			int index = ((((TimeZoneListAdapter) grid.getAdapter()).getIndex() - 1) * VIEW_COUNT)
					+ info.position;
			LogUtil.D("" + index);
			grid.setSelection(index);
			if (demopw == null) {
				demopw = new MenuPopup(this);
				demopw.show(this.findViewById(R.id.test_demo), 3, 3, index);
			} else {
				demopw.show(this.findViewById(R.id.test_demo), 3, 3, index);
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// If subactivity has resulted in a timezone selection, restart task to
		// get new data.
		if (resultCode == RESULT_OK) {
			GetInfoTask getInfoTask = new GetInfoTask();
			getInfoTask.execute();
		}
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		if (demopw != null && demopw.isShowing()) {
			return demopw.dispatchKeyEvent(event);
		} else if (event.getKeyCode() == KeyEvent.KEYCODE_MENU
				&& event.getAction() == KeyEvent.ACTION_DOWN) {
			int index = -1;
			try {
				index = ((((TimeZoneListAdapter) grid.getAdapter()).getIndex() - 1) * VIEW_COUNT)
						+ grid.getSelectedItemPosition();
			} catch (Exception e) {
				LogUtil.printStackTrace(e);
			}
			if (demopw == null)
				demopw = new MenuPopup(this);
			demopw.show(this.findViewById(R.id.test_demo), 3, 3, index);
		}
		return super.dispatchKeyEvent(event);
	}

	public void sendMessage(int msgCode, Object o) {
		Message m = new Message();
		m.what = msgCode;
		m.obj = o;
		mHandler.sendMessage(m);
	}

	public class GetInfoTask extends AsyncTask<String, Void, List<String>> {
		@Override
		protected List<String> doInBackground(String... params) {

			List<String> result = new ArrayList<String>();
			WorldClockProvider dbHelper = new WorldClockProvider(
					WorldClockList.this);
			dbHelper.open();
			result = dbHelper.getAllLocations();
			dbHelper.close();
			return result;
		}

		@Override
		protected void onPostExecute(final List<String> result) {
			onGetInfoResult(result);
		}

	}

	/**
	 * Menu for world clock list
	 * 
	 * @author Gajah Digital Media
	 * 
	 */
	public class MenuPopup implements OnKeyListener {
		public PopupWindow demopopup;
		private ListView mListview;
		private View inflateView;

		private final int ADD_NEW_LOCATION = 0;
		private final int DELETE_LOCATION = 1;

		private int selected_index = -1;

		public MenuPopup(Activity context) {
			LayoutInflater inflate = LayoutInflater.from(context);
			inflateView = inflate.inflate(R.layout.menu_weather_list, null);
			mListview = (ListView) inflateView
					.findViewById(R.id.menu_popupwindow_listview);
			demopopup = new PopupWindow(inflateView, 300,
					LayoutParams.WRAP_CONTENT);
			demopopup.setFocusable(true);
		}

		public void initData() {
			String[] items;
			if (selected_index > -1) {
				items = getResources().getStringArray(
						R.array.menu_worldclock_list);
			} else {
				items = getResources().getStringArray(
						R.array.menu_worldclock_list_1);
			}
			ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(
					WorldClockList.this, R.layout.menu_list_item, items);
			mListview.setAdapter(listAdapter);
			mListview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			mListview.clearChoices();
			mListview.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					mListview.setItemChecked(position, true);
					dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN,
							KeyEvent.KEYCODE_DPAD_CENTER));
				}

			});
			mListview.setOnKeyListener(this);
		}

		public void show(View parent, int x, int y, int index) {
			selected_index = index;
			initData();
			LogUtil.e("Menu Pop Up", index + "");
			demopopup.showAtLocation(parent, Gravity.NO_GRAVITY, x, y);

		}

		public boolean isShowing() {
			if (demopopup != null)
				return demopopup.isShowing();
			return false;
		}

		public void dismiss() {
			if (demopopup != null)
				demopopup.dismiss();

		}

		public boolean dispatchKeyEvent(KeyEvent event) {
			LogUtil.I("----------mListview::dispatchKeyEvent-----------"
					+ event.getKeyCode());
			if (event.getKeyCode() == KeyEvent.KEYCODE_BACK
					&& event.getAction() == KeyEvent.ACTION_DOWN) {
				dismiss();
				return true;
			} else if (event.getKeyCode() == KeyEvent.KEYCODE_MENU
					&& event.getAction() == KeyEvent.ACTION_DOWN) {
				dismiss();
				return true;
			} else if ((event.getKeyCode() == KeyEvent.KEYCODE_DPAD_CENTER)
					&& event.getAction() == KeyEvent.ACTION_DOWN) {

				int index = mListview.getCheckedItemPosition();
				if (index < 0) {
					index = mListview.getSelectedItemPosition();
				}
				LogUtil.E("Selected ID is " + index);
				switch (index) {
				case ADD_NEW_LOCATION:
					dismiss();
					Intent newIntent = new Intent(WorldClockList.this,
							TimeZoneList.class);
					startActivityForResult(newIntent, 0);
					break;

				case DELETE_LOCATION:
					dismiss();
					WorldClockProvider dbHelper = new WorldClockProvider(
							WorldClockList.this);
					dbHelper.open();
					dbHelper.deleteLocation(timeZoneStringList
							.get(selected_index));
					dbHelper.close();
					GetInfoTask getInfoTask = new GetInfoTask();
					getInfoTask.execute();
					break;

				}
				return true;
			}
			return false;
		}

		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			return dispatchKeyEvent(event);
		}
	}

	public void onGetInfoResult(final List<String> result) {
		mdata.clear();
		timeZoneStringList.clear();
		if (result != null & result.size() > 0) {
			tvError.setText("");
			LogUtil.i(TAG, result.size() + "Size of ");

			for (int i = 0; i < result.size(); i++) {
				LogUtil.d(TAG, result.get(i).toString());
				TimeZone tz = TimeZone.getTimeZone(result.get(i));
				HashMap<String, Object> item = new HashMap<String, Object>();
				item.put(LOCATION, getDiaplayName(result.get(i)));
				item.put(TIMEZONE, tz);
				item.put(TIMEZONE1, tz);
				mdata.add(item);
				timeZoneStringList.add(result.get(i));
			}
			listAdapter = new TimeZoneListAdapter(WorldClockList.this, mdata,
					R.layout.world_clock_grid_item, new String[] { LOCATION,
							TIMEZONE, TIMEZONE1 }, VIEW_COUNT);
			grid.setAdapter(listAdapter);
			Object o = listAdapter.getIndex()
					+ "/"
					+ +(listAdapter.getTotalpage() > 0 ? listAdapter
							.getTotalpage() : 1);

			sendMessage(MSG_UPDATE_PAGE_NUM, o);
		} else {
			if (listAdapter != null) {
				listAdapter.notifyDataSetChanged();
			}
			tvError.setText(getText(R.string.error_no_location));
		}

	}

	private String getDiaplayName(String timezonename) {
		String displayname = timezonename;
		int sep = timezonename.indexOf('/');

		if (-1 != sep) {
			displayname = timezonename.substring(sep + 1) + ", "
					+ timezonename.substring(0, sep);
			displayname = displayname.replace("_", " ");
		}

		return displayname;
	}

}
