/* Copyright (C) 2012 Gajah Digital Media
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.gajah.einkworldclock;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.gajah.einkworldclock.provider.WorldClockProvider;

public class WorldClockApplication extends Application {
	

	@Override
	public void onCreate() {
		super.onCreate();
		if (isFirstTime(this)) {
			LogUtil.i("World Clock App first time", "true");
			WorldClockProvider dbHelper = new WorldClockProvider(this);
			dbHelper.open();
			dbHelper.createLocation("America/New_York");
			dbHelper.createLocation("Asia/Tokyo");
			dbHelper.createLocation("Europe/London");
			dbHelper.createLocation("Asia/Hong_Kong");
			dbHelper.close();
			setIsFirstTime(false, this);
		}

	}

	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	private static final String TOKEN_FIRST_TIME = "first-time";
	private static final String KEY = "world-clock-pref";

	public static boolean setIsFirstTime(boolean firsttime, Context context) {
		Editor editor = context.getSharedPreferences(KEY, Context.MODE_PRIVATE)
				.edit();
		editor.putBoolean(TOKEN_FIRST_TIME, firsttime);
		return editor.commit();
	}

	public static boolean isFirstTime(Context context) {
		SharedPreferences savedSession = context.getSharedPreferences(KEY,
				Context.MODE_PRIVATE);
		return savedSession.getBoolean(TOKEN_FIRST_TIME, true);
	}
}
